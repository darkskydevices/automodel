# AutoModel

Create and run point models on your 10Micron mount to get the best possible alignment to the sky.

![](images/make_tab.png)

## Getting Started

AutoModel is currently Windows only and requires Python, ASCOM, and PlateSolve2.


### Prerequisites

AutoModel requires the following:

 * Windows 7/8.1/10
 * [ASCOM 6.4 SP1](https://ascom-standards.org/)
 * [PlaneWave Instruments PlateSolve v2.28 [Direct Download]](http://planewave.com/downloads/get/25)
 * [PlaneWave Instruments UCAC3PS Catalog [Direct Download]](http://planewave.com/downloads/get/107)

### Installing

See the [Releases](https://gitlab.com/darkskydevices/automodel/-/releases) page to download the latest release version. Or, you can clone the repo and run `src/main.py`.

AutoModel is released as a single executable for easy portability, but there are some prerequisites that must be completed before you can start modeling.

1. Download the latest AutoModel executable and the prerequisites listed above.
2. Extract PlateSolve2 to My Documents. You will need to point AutoModel to this folder later.
3. Extract UCAC3PS and place in `My Documents/PlateSolve2.28` folder.
4. Run PlateSolve2 and configure the UCAC3PS catalog.
5. Run AutoModel and on the Settings tab set the PS2 path to the `My Documents/PlateSolve2.28` directory.

### Creating a Model

AutoModel generates models with 3 base points and many refine points. Specify the azimuth and altitude for the base points. Next, for best results use the equidistant point generation method. Specify the number of points, minimum, and maximum altitude, then generate and save the model file.

### Running the Model

On the Model tab, load the the model you just created and verify the points make sense. Connect to your mount and camera and press start! AutoModel will clear the model on the mount, visit the 3 base points followed by the refine points in the order that they are defined in the model.

PlateSolve2 results are in J2000 / ICRS coordinates but 10Micron mounts want Observed / JNow coordinates. AutoModel automatically makes this conversion so your model will be accurate.

## Bug Reports

Open an issue and describe with as much detail as you can the issue you are having. All issues will be triaged but they may or may not be included in a future release.

## Contributing

Open an issue prior to developing a feature or bug fix. All features and bug fixes may not be accepted.

## Versioning

We use [SemVer](http://semver.org/) for versioning. 

## Authors

* **Alex Helms**

## License

This project is licensed under the LGPLv3 License - see the [LICENSE](LICENSE) file for details