# -*- mode: python -*-

block_cipher = None

import os
import sys
import ntpath
import PyQt5
import astropy

qt_path = os.path.join(ntpath.dirname(PyQt5.__file__), 'Qt', 'bin')
astropy_path = astropy.__path__[0]

a = Analysis(['src\\main.py'],
             pathex=['src', 'src\\automodel', qt_path],
             binaries=[],
             datas=[(astropy_path, 'astropy')],
             hiddenimports=['fractions', 'shelve', 'numpy.lib.recfunctions', 'csv', 'pkg_resources.py2_warn'],
             hookspath=[],
             runtime_hooks=[],
             excludes=['astropy'],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)

a.datas += [('./assets/star-face.png', 'src/automodel/assets/star-face.png', 'DATA')]

pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='AutoModel',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=False,
          runtime_tmpdir=None,
          console=False,
          icon='src\\automodel\\assets\\star-face.ico')
