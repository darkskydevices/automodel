from PyQt5.QtCore import QSettings


class Settings(object):
    def __init__(self,
                 settings: QSettings):
        self.settings = settings

    ###########
    # General #
    ###########

    @property
    def last_save_directory(self):
        return str(self.settings.value("general/last_save_directory", ""))

    @last_save_directory.setter
    def last_save_directory(self, value):
        self.settings.setValue("general/last_save_directory", value)

    ################
    # Plate Solver #
    ################

    @property
    def ps2_directory(self) -> str:
        return str(self.settings.value("ps2/directory", ""))

    @ps2_directory.setter
    def ps2_directory(self, value: str):
        self.settings.setValue("ps2/directory", value)

    #########
    # Mount #
    #########

    @property
    def mount_id(self) -> str:
        return str(self.settings.value("mount/id"))

    @mount_id.setter
    def mount_id(self, value: str):
        self.settings.setValue("mount/id", value)

    @property
    def mount_settle_time(self) -> float:
        return float(self.settings.value("mount/settle_time", 0))

    @mount_settle_time.setter
    def mount_settle_time(self, value: float):
        self.settings.setValue("mount/settle_time", value)

    ##########
    # Camera #
    ##########

    @property
    def camera_id(self) -> str:
        return str(self.settings.value("camera/id"))

    @camera_id.setter
    def camera_id(self, value: str):
        self.settings.setValue("camera/id", value)

    @property
    def camera_exposure(self) -> float:
        return float(self.settings.value("camera/exposure", 1.0))

    @camera_exposure.setter
    def camera_exposure(self, value: float):
        self.settings.setValue("camera/exposure", value)

    @property
    def camera_binning(self) -> int:
        return int(self.settings.value("camera/binning", 1))

    @camera_binning.setter
    def camera_binning(self, value: int):
        self.settings.setValue("camera/binning", value)

    @property
    def camera_scale(self) -> float:
        return float(self.settings.value("camera/scale", 1.0))

    @camera_scale.setter
    def camera_scale(self, value: float):
        self.settings.setValue("camera/scale", value)

    @property
    def camera_temperature(self):
        return float(self.settings.value("camera/temperature", -10.0))

    @camera_temperature.setter
    def camera_temperature(self, value: float):
        self.settings.setValue("camera/temperature", value)

    ###########
    # General #
    ###########

    @property
    def temperature(self):
        return int(self.settings.value("general/temperature", 15))

    @temperature.setter
    def temperature(self, value):
        self.settings.setValue("general/temperature", value)

    @property
    def pressure(self):
        return int(self.settings.value("general/pressure", 1013))

    @pressure.setter
    def pressure(self, value):
        self.settings.setValue("general/pressure", value)
