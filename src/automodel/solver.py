import math
import os.path
import subprocess
from automodel.solverResults import SolveResults
from astropy.coordinates import Angle
from astropy import units as u


class PS2Solver(object):

    def __init__(self, ps2_directory):
        self.ps2Directory = ps2_directory

    def _file_exists(self, filename):
        return os.path.exists(filename)

    def _delete_apm_file(self, filename):
        if os.path.exists(filename):
            try:
                os.remove(filename)
            except OSError as e:
                print("Error: %s - %s." % (e.filename, e.strerror))

    def resolve_ps2(self):
        if not os.path.isdir(self.ps2Directory):
            print("Invalid PS2 directory")
            return False

        if not os.path.exists(self.ps2Directory):
            print("PS2 directory does not exist")
            return False

        if not self._file_exists(os.path.join(self.ps2Directory, "PlateSolve2.exe")):
            print("PlateSolve2.exe not found")
            return False

        if not self._file_exists(os.path.join(self.ps2Directory, "PlateSolve2.ini")):
            print("PlateSolve2.ini not found")
            return False

        if not self._file_exists(os.path.join(self.ps2Directory, "comdlg32.ocx")):
            print("comdlg32.ocx not found")
            return False

        return True

    def solve(self, ra, dec, width, height, fits_file):
        """Solves a star field

        Args:
            ra: RA guess of center in degrees
            dec: DEC guess of center in degrees
            width: FOV width in degrees
            height: FOV height in degrees
            fits_file: Filename of FITS image to solve

        Returns:
            SolveResults or None

        """

        # Check PS2 directory and executables exist
        if not self.resolve_ps2():
            return None

        # PS2 requires the following cmd args
        # 1. RA in radians
        # 2. DEC in radians
        # 3. FOV width in radians
        # 4. FOV height in radians
        # 5. Number of sectors to try
        # 6. FITS filename of image to solve
        # 7. Time in seconds to stay open after solve

        # Produces a file with the same name as the FITS file but extension
        # is .apm.

        deg2_rad = math.pi / 180.0

        ra *= deg2_rad
        dec *= deg2_rad
        width *= deg2_rad
        height *= deg2_rad

        arg = "%.5f,%.5f,%.5f,%.5f,1000,%s,0" % (ra, dec, width, height, fits_file)

        # executing ps2 directly with python
        # doesnt allow for spaces in the fits
        # file name for some reason. Using
        # powershell bypasses this issue.
        # Out-Null forces powershell to wait
        # for the exe to finish before returning
        ps2File = os.path.join(self.ps2Directory, "PlateSolve2.exe")
        subprocess.run('"' + ps2File + '" ' + arg)

        # Parse the apm file
        # *.apm file contents:
        # 1.03386718,0.65199823,1
        # 2.26075,322.55,-0.99965,0.00003,297
        # Valid plate solution
        #
        # Line 1
        # RA * 15 in radians, DEC in radians, statis
        #
        # NOTE: status is -2, -1, 0, or 1. This changes the value of line 3
        # 1  = "Valid solve."
        # 0  = "Solve failed!  Unanticipated problem during plate matching."
        # -1 = "Solve failed!  Maximum search limit exceeded."
        # -2 = "Solve failed!  User aborted."
        #
        # Line 2
        # image scale in arcsec/px, angle in degrees, parity, unknown, number stars found
        #
        # PARITY MAY NOT BE CORRECT
        # NOTE: Parity, if < 0, angle = 360-angle and flipped=false
        #              if > 0, angle = angle-=180, if angle < 0, angle+=360, flipped=true
        #
        # Line 3
        # "Solve failed!  User aborted."
        # "Solve failed!  Maximum search limit exceeded."
        # "Solve failed!  Unanticipated problem during plate matching."
        # "Valid solve."

        pre, ext = os.path.splitext(fits_file)
        apm_filename = pre + ".apm"

        if self._file_exists(apm_filename):
            try:
                # PS2 created an empty file but failed
                if os.path.getsize(apm_filename) == 0:
                    return None

                with open(apm_filename) as f:
                    apm_lines = f.readlines()

                if "Valid" not in apm_lines[2]:
                    return None

                line1 = apm_lines[0].split(',')
                line2 = apm_lines[1].split(',')

                result = SolveResults()
                result.ra = float(line1[0]) / 15.0 * 180.0 / math.pi
                result.dec = float(line1[1]) * 180.0 / math.pi
                result.scale = float(line2[0])
                result.angle = float(line2[1])
                result.parity = float(line2[2]) < 0  # less than 0 means flipped about Y axis
                return result
            finally:
                self._delete_apm_file(apm_filename)

    @staticmethod
    def print_solver_results(results):
        ra = Angle(results.ra, u.hour)
        dec = Angle(results.dec, u.deg)

        ra_h, ra_m, ra_s = ra.hms
        dec_d, dec_m, dec_s = dec.dms

        msg = ""
        msg += f"(J2000)      RA: {int(ra_h):02d}h {int(ra_m):02d}m {ra_s:05.2f}s\n"
        msg += f"(J2000)     DEC: {int(dec_d):02d}d {int(dec_m):02d}m {dec_s:05.2f}s\n"
        msg += f"(deg)     Angle: {results.angle:05.2f}\n"
        msg += f"(as/px)   Scale: {results.scale:05.2f}\n"
        msg += f"        Flipped: {results.parity and 'Y' or 'N'}"
        return msg
