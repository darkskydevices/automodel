# Versioning variables
semver_major = 1
semver_minor = 1
semver_patch = 0
semver = f"{semver_major}.{semver_minor}.{semver_patch}"
