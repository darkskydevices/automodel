import time
from threading import Thread, Lock
from PyQt5.QtCore import QObject, pyqtSignal
from automodel.astrometry import Astrometry
from astropy import units as u
from enum import IntEnum
from win32com.client.dynamic import Dispatch
import pythoncom


class ModelMode(IntEnum):
    BASE = 0
    REFINE = 1


class MountPollingData(object):
    def __init__(self):
        self.is_connected = False
        self.alt = 0.0
        self.az = 0.0
        self.ra = 0.0
        self.dec = 0.0
        self.lat = 0.0
        self.lon = 0.0
        self.elv = 0.0
        self.lst = 0.0


class Mount(QObject):
    TENMICRON_PROG_ID = "ASCOM.tenmicron_mount.Telescope"
    SIMULATOR_PROG_ID = "ASCOM.Simulator.Telescope"

    data_signal = pyqtSignal(MountPollingData)

    def __init__(self, prog_id):
        super().__init__()
        self.prog_id = prog_id
        self.ascom = None
        self._ascom_lock = Lock()
        self._poller = None
        self._poll_interval = 0.5

    def _poll(self):
        # This method is dispatched on another thread
        # CoInitialize must be called to setup the thread
        # for Win32 COMObjects
        pythoncom.CoInitialize()

        while self.ascom:
            data = MountPollingData()
            data.is_connected = self.is_connected
            data.alt = self.alt
            data.az = self.az
            data.ra = self.ra
            data.dec = self.dec
            data.lat = self.latitude
            data.lon = self.longitude
            data.elv = self.elevation
            data.lst = self.lst
            self.data_signal.emit(data)
            time.sleep(self._poll_interval)

        self.poller = None

    def open(self):
        self.ascom = Dispatch(self.prog_id)
        self.ascom.Connected = True

        self._poller = Thread(target=self._poll)
        self._poller.daemon = True
        self._poller.start()

    def close(self):
        self.ascom.Connected = False
        self.ascom.Dispose()
        self.ascom = None

    def slew_altaz(self, alt, az):
        if not self.is_connected:
            return
        self.ascom.SlewToAltAz(az, alt)

    def slew_radec(self, ra, dec):
        if not self.is_connected:
            return
        self.ascom.SlewToCoordinates(ra, dec)

    def add_model_point_icrs(self, ra, dec):
        if not self.is_connected:
            return
        coord = Astrometry.transform_icrs_to_cirs(ra, dec)
        cmd_ra = "Sr%s" % coord.ra.to_string(sep="::", precision=2, unit=u.hour)
        cmd_dec = "Sd%s" % coord.dec.to_string(sep="*:", precision=2, unit=u.deg)
        cmd_add = "CMS"

        res = self._send_command_bool(cmd_ra)
        if not res:
            print("Error sending " + cmd_ra + ", received " + res)

        res = self._send_command_bool(cmd_dec)
        if not res:
            print("Error sending " + cmd_dec + ", received " + res)

        res = self._send_command_string(cmd_add)
        if res.strip() != "V#":
            print("Error sending " + cmd_add + ", received " + res)

    def set_mode(self, mode: ModelMode):
        cmd = "CMCFG%1d" % int(mode)
        res = self._send_command_string(cmd)
        if res.strip() != ("%1d#" % int(mode)):
            print("Error sending " + cmd + ", received " + res)

    def clear_model(self):
        self._send_command_string("delalig")

    def get_point_info(self, num):
        cmd = "getalp%d" % num
        res = self._send_command_string(cmd)
        if res == "E#":
            print("Error sending " + cmd + ", received " + res)
            print("number out of range")
            return None
        values = res.split(",")
        error = float(values[2].strip())
        polar_angle = int(values[3].strip().rstrip("#"))
        return error, polar_angle

    def set_temperature_pressure(self, temperature, pressure):
        temp_cmd = f"SRTMP{temperature:.1f}"
        pres_cmd = f"SRPRS{pressure:.1f}"

        res = self._send_command_bool(temp_cmd)
        if not res:
            print("Error sending " + temp_cmd + ", received " + res)

        res = self._send_command_bool(pres_cmd)
        if not res:
            print("Error sending " + pres_cmd + ", received " + res)

    def park(self):
        self._send_command_string_blind("KA")

    def unpark(self):
        self._send_command_string_blind("PO")

    def _send_command_string(self, cmd):
        if self.is_connected:
            with self._ascom_lock:
                return self.ascom.CommandString(cmd, False)

    def _send_command_bool(self, cmd):
        if self.is_connected:
            with self._ascom_lock:
                return self.ascom.CommandBool(cmd, False)

    def _send_command_string_blind(self, cmd):
        if self.is_connected:
            with self._ascom_lock:
                return self.ascom.CommandBlind(cmd, False)

    @property
    def is_connected(self):
        return self.ascom and self.ascom.Connected

    @property
    def ra(self):
        return self.ascom.RightAscension if self.is_connected else 0.0

    @property
    def dec(self):
        return self.ascom.Declination if self.is_connected else 0.0

    @property
    def alt(self):
        return self.ascom.Altitude if self.is_connected else 0.0

    @property
    def az(self):
        return self.ascom.Azimuth if self.is_connected else 0.0

    @property
    def latitude(self):
        return self.ascom.SiteLatitude if self.is_connected else 0.0

    @property
    def longitude(self):
        return self.ascom.SiteLongitude if self.is_connected else 0.0

    @property
    def elevation(self):
        return self.ascom.SiteElevation if self.is_connected else 0.0

    @property
    def lst(self):
        return self.ascom.SiderealTime if self.is_connected else 0.0

    @property
    def num_model_stars(self):
        if not self.is_connected:
            return None
        res = self._send_command_string("getalst")
        return int(res.rstrip('#').strip())
