import os
import math
from time import localtime, strftime

import PyQt5
from PyQt5 import QtGui
from PyQt5.QtWidgets import QMainWindow, QFileDialog, QMessageBox

from automodel.version import semver
from automodel.util import resource_path
from automodel.ui import ui_mainwindow
from automodel.solver import PS2Solver
from automodel.context import Context
from automodel.modelGenerator import ModelGenerator, ModelType
from automodel.modelPoint import ModelPoint
from automodel.astrometry import Astrometry

from win32com.client.dynamic import Dispatch


class MainWindow(QMainWindow):
    ICON_FILENAME = "./assets/star-face.png"
    CHOOSER_PROG_ID = "ASCOM.Utilities.Chooser"
    DEVICE_TELESCOPE = "Telescope"
    DEVICE_CAMERA = "Camera"
    DEFAULT_SAVE_DIRECTORY = "~/Desktop"
    DEFAULT_FILENAME = "model.mdl"

    def __init__(self,
                 context: Context):
        QMainWindow.__init__(self)
        self.ui = ui_mainwindow.Ui_MainWindow()
        self.ui.setupUi(self)

        self.context = context
        self.settings = context.settings

        self._generated_points = []
        self._loaded_points = []

        self._set_version()
        self._set_icon()
        self._wire_slots()
        self._restore_from_settings()

        font = QtGui.QFont("Monospace")
        font.setStyleHint(QtGui.QFont.TypeWriter)
        font.setPointSize(9)
        self.ui.tbStatus.setCurrentFont(font)

        # Temperature ane pressure are not yet used
        self.ui.lblSettingsTemperature.hide()
        self.ui.nudTemperature.hide()
        self.ui.lblSettingsPressure.hide()
        self.ui.nudPressure.hide()

    def _set_version(self):
        title = self.windowTitle() + " " + semver
        self.setWindowTitle(title)

    def _set_icon(self):
        icon_filename = resource_path(self.ICON_FILENAME)
        self.setWindowIcon(QtGui.QIcon(icon_filename))

    def _wire_slots(self):
        # Settings
        self.ui.btnPs2Path.clicked.connect(self.select_ps2_directory)
        self.ui.btnChooseMount.clicked.connect(self.choose_mount)
        self.ui.nudMountSettleTime.valueChanged.connect(self.set_mount_settle_time)
        self.ui.btnChooseCamera.clicked.connect(self.choose_camera)
        self.ui.nudCameraExposure.valueChanged.connect(self.set_camera_exposure)
        self.ui.nudCameraBinning.valueChanged.connect(self.set_camera_binning)
        self.ui.nudCameraScale.valueChanged.connect(self.set_camera_scale)
        self.ui.nudCameraTemperature.valueChanged.connect(self.set_camera_temperature)
        self.ui.nudTemperature.valueChanged.connect(self.set_temperature)
        self.ui.nudPressure.valueChanged.connect(self.set_pressure)

        # Make
        self.ui.btnGenerateModel.clicked.connect(self.generate_points)
        self.ui.btnSaveModel.clicked.connect(self.save_generated_points)

        # Root
        self.ui.btnConnect.clicked.connect(self.connect_gear)
        self.ui.btnDisconnect.clicked.connect(self.disconnect_gear)

        # Model
        self.ui.btnOpenModel.clicked.connect(self.open_model)
        self.ui.btnStart.clicked.connect(self.start_modeling)
        self.ui.btnStop.clicked.connect(self.stop_modeling)
        self.ui.btnClearLog.clicked.connect(self.clear_log)

    def _restore_from_settings(self):
        self.ui.editPs2Path.setText(self.settings.ps2_directory)
        self.ui.lblMountId.setText(self.settings.mount_id)
        self.ui.lblCameraId.setText(self.settings.camera_id)
        self.ui.nudCameraExposure.setValue(self.settings.camera_exposure)
        self.ui.nudCameraBinning.setValue(self.settings.camera_binning)
        self.ui.nudCameraScale.setValue(self.settings.camera_scale)
        self.ui.nudCameraTemperature.setValue(self.settings.camera_temperature)
        self.ui.nudTemperature.setValue(self.settings.temperature)
        self.ui.nudPressure.setValue(self.settings.pressure)
        self.ui.nudMountSettleTime.setValue(self.settings.mount_settle_time)

    def _get_default_model_directory(self):
        home_path = self.settings.last_save_directory
        if not home_path:
            home_path = os.path.expanduser(self.DEFAULT_SAVE_DIRECTORY)
        return home_path

    def _get_default_model_filename(self):
        home_path = self._get_default_model_directory()
        return os.path.join(home_path, self.DEFAULT_FILENAME)

    def _get_base_points_from_ui(self):
        p1 = ModelPoint(self.ui.nudBasePoint1Alt.value(), self.ui.nudBasePoint1Az.value(), True)
        p2 = ModelPoint(self.ui.nudBasePoint2Alt.value(), self.ui.nudBasePoint2Az.value(), True)
        p3 = ModelPoint(self.ui.nudBasePoint3Alt.value(), self.ui.nudBasePoint3Az.value(), True)
        return p1, p2, p3

    @staticmethod
    def _test_ps2_directory(folder):
        solver = PS2Solver(folder)
        return solver.resolve_ps2()

    @staticmethod
    def _add_points_to_listbox(listbox, point):
        def create_label(pnt, cnt):
            return f"Point {cnt:2d}: {pnt}"

        listbox.clear()

        # Add the refine points
        count = 1
        for p in point:
            prefix = "Base" if p.is_base else "Refine"
            label = prefix + " " + create_label(p, count)
            listbox.addItem(label)
            count += 1

    def closeEvent(self, event: PyQt5.QtGui.QCloseEvent):
        self.context.close()
        event.accept()

    def update_mount_ui(self, data):
        self.ui.lblAltitude.setText(Astrometry.degrees_to_dms(data.alt))
        self.ui.lblAzimuth.setText(Astrometry.degrees_to_dms(data.az))
        self.ui.lblRightAscension.setText(Astrometry.hours_to_hms(data.ra))
        self.ui.lblDeclination.setText(Astrometry.degrees_to_dms(data.dec))
        self.ui.lblLatitude.setText(Astrometry.degrees_to_dms(data.lat))
        self.ui.lblLongitude.setText(Astrometry.degrees_to_dms(data.lon))
        self.ui.lblElevation.setText(f"{data.elv:.0f}")
        self.ui.lblLocalSiderealTime.setText(Astrometry.hours_to_hms(data.lst))

        if data.is_connected:
            self.ui.lblMountConnectionStatus.setStyleSheet("background-color: darkgreen; color: white;")
        else:
            self.ui.lblMountConnectionStatus.setStyleSheet("background-color: rgb(85,0,0); color: white;")

    def clear_mount_ui(self):
        self.ui.lblAltitude.setText("--")
        self.ui.lblAzimuth.setText("--")
        self.ui.lblRightAscension.setText("--")
        self.ui.lblDeclination.setText("--")
        self.ui.lblLatitude.setText("--")
        self.ui.lblLongitude.setText("--")
        self.ui.lblElevation.setText("--")
        self.ui.lblLocalSiderealTime.setText("--")
        self.ui.lblMountConnectionStatus.setStyleSheet("background-color: rgb(85,0,0); color: white;")

    def update_camera_ui(self, data):
        if data.is_connected:
            self.ui.lblCameraConnectionStatus.setStyleSheet("background-color: darkgreen; color: white;")
            if not math.isnan(data.temperature):
                self.ui.lblCameraTemperature.setText(f'({data.temperature:.1f}°C)')
            else:
                self.ui.lblCameraTemperature.setText('')
        else:
            self.ui.lblCameraConnectionStatus.setStyleSheet("background-color: rgb(85,0,0); color: white;")
            self.ui.lblCameraTemperature.setText('')

    def clear_camera_ui(self):
        self.ui.lblCameraConnectionStatus.setStyleSheet("background-color: rgb(85,0,0); color: white;")
        self.ui.lblCameraTemperature.setText('')

    def select_ps2_directory(self):
        folder = str(QFileDialog.getExistingDirectory(self, "Select PlateSolve2 Folder"))
        if not folder:
            return
        if self._test_ps2_directory(folder):
            self.settings.ps2_directory = folder
            self.ui.editPs2Path.setText(self.settings.ps2_directory)
        else:
            QMessageBox.warning(self, "Solver", "Cannot find PlateSolve2", QMessageBox.Ok)

    def choose_mount(self):
        chooser = Dispatch(self.CHOOSER_PROG_ID)
        try:
            chooser.DeviceType = self.DEVICE_TELESCOPE
            mount_id = chooser.Choose(self.settings.mount_id)
            if mount_id:
                self.settings.mount_id = mount_id
                self.ui.lblMountId.setText(mount_id)
        except Exception as e:
            print("Exception choosing mount: ", e)

    def set_mount_settle_time(self, value):
        self.settings.mount_settle_time = value

    def choose_camera(self):
        chooser = Dispatch(self.CHOOSER_PROG_ID)
        try:
            chooser.DeviceType = self.DEVICE_CAMERA
            camera_id = chooser.Choose(self.settings.camera_id)
            if camera_id:
                self.settings.camera_id = camera_id
                self.ui.lblCameraId.setText(camera_id)
        except Exception as e:
            print("Exception choosing camera: ", e)

    def set_camera_exposure(self, value):
        self.settings.camera_exposure = value

    def set_camera_binning(self, value):
        self.settings.camera_binning = value

    def set_camera_scale(self, value):
        self.settings.camera_scale = value

    def set_camera_temperature(self, value):
        self.settings.camera_temperature = value

    def set_temperature(self, value):
        self.settings.temperature = value

    def set_pressure(self, value):
        self.settings.pressure = value

    def generate_points(self):
        base_points = list(self._get_base_points_from_ui())

        if self.ui.rbEquidistant.isChecked():
            self._generated_points = base_points + self._generate_equidistant_points()
        elif self.ui.rbGrid.isChecked():
            self._generated_points = base_points + self._generate_grid_points()

        self._generated_points.sort(key=lambda x: x.az)
        self._add_points_to_listbox(self.ui.listModelOutput, self._generated_points)
        self.ui.lblNumGeneratedPoints.setText(f"{len(self._generated_points)} Points")

    def _generate_equidistant_points(self):
        g = ModelGenerator(ModelType.EQUIDISTANT)
        g.num = self.ui.nudEquidistantPoints.value()
        g.min_alt = self.ui.nudRefinePointMinAlt.value()
        g.min_az = self.ui.nudRefinePointMinAz.value()
        g.max_alt = self.ui.nudRefinePointMaxAlt.value()
        g.max_az = self.ui.nudRefinePointMaxAz.value()
        return list(g.generate())

    def _generate_grid_points(self):
        g = ModelGenerator(ModelType.CARTESIAN)
        g.rows = self.ui.nudRefinePointRows.value()
        g.cols = self.ui.nudRefinePointColumns.value()
        g.min_alt = self.ui.nudRefinePointMinAlt.value()
        g.min_az = self.ui.nudRefinePointMinAz.value()
        g.max_alt = self.ui.nudRefinePointMaxAlt.value()
        g.max_az = self.ui.nudRefinePointMaxAz.value()
        return list(g.generate())

    def save_generated_points(self):
        if not self._generated_points or not len(self._generated_points):
            return

        filename, _ = QFileDialog.getSaveFileName(self,
                                                  "Save Model",
                                                  self._get_default_model_filename(),
                                                  "AutoModel Model (*.mdl)")
        if filename:
            try:
                ModelPoint.save(self._generated_points, filename)
                self.settings.last_save_directory = os.path.dirname(os.path.abspath(filename))
            except Exception as ex:
                print(ex)
                QMessageBox.critical(self, "Save Model", "An error occurred saving the model")

    def connect_gear(self):
        if self.settings.mount_id and not self.context.mount:
            try:
                self.context.create_mount(self.settings.mount_id)
                self.context.mount.open()
                self.context.mount.data_signal.connect(self.update_mount_ui)
            except Exception as ex:
                print(ex)
                QMessageBox.critical(self, "Mount", "Failed to connect to mount")

        if self.settings.camera_id:
            try:
                self.context.create_camera(self.settings.camera_id)
                self.context.camera.open()
                self.context.camera.data_signal.connect(self.update_camera_ui)

                if self.context.camera.has_cooler:
                    self.context.camera.set_temperature(self.settings.camera_temperature)
            except Exception as ex:
                print(ex)
                QMessageBox.critical(self, "Camera", "Failed to connect to camera")

    def disconnect_gear(self):
        if self.context.camera:
            self.context.camera.close()
            self.context.camera = None
            self.clear_camera_ui()

        if self.context.mount:
            self.context.mount.close()
            self.context.mount = None
            self.clear_mount_ui()

    def open_model(self):
        filename, _ = QFileDialog.getOpenFileName(self,
                                                  "Open Model",
                                                  self._get_default_model_directory(),
                                                  "AutoModel Model (*.mdl)")
        if filename:
            try:
                self._loaded_points = list(ModelPoint.load(filename))
                self.ui.editLoadedModelFilename.setText(filename)
                self._add_points_to_listbox(self.ui.listModelLoaded, self._loaded_points)
                self.append_log(f"Loaded {len(self._loaded_points)} points from {filename}")
            except Exception as ex:
                print(ex)
                QMessageBox.critical(self, "Open Model", "An error occurred opening the model")

    def clear_log(self):
        self.ui.tbStatus.clear()

    def append_log(self, text):
        timestamp = strftime("%H:%M:%S", localtime())
        self.ui.tbStatus.insertPlainText(f"{timestamp} {text.strip()}\n")

        sb = self.ui.tbStatus.verticalScrollBar()
        sb.setValue(sb.maximum())

    def start_modeling(self):
        if not self.context.mount or not self.context.mount.is_connected:
            QMessageBox.critical(self, "Start Modeling", "Mount is not connected")
            return
        if not self.context.camera or not self.context.camera.is_connected:
            QMessageBox.critical(self, "Start Modeling", "Camera is not connected")
            return
        if self.context.automation and self.context.automation.is_running:
            QMessageBox.critical(self, "Start Modeling", "Modeling is already running")
            return
        if not self._test_ps2_directory(self.settings.ps2_directory):
            QMessageBox.critical(self, "Start Modeling", "PlateSolve2 not found")
            return
        if not len(self._loaded_points):
            QMessageBox.critical(self, "Start Modeling", "No model has been loaded")
            return
        if len(self._loaded_points) < 3:
            QMessageBox.critical(self, "Start Modeling", "Insufficient model points, 3 base points are required")
            return

        if not self.context.automation:
            self.context.create_solver(self.settings.ps2_directory)
            self.context.create_automation()
            self.context.automation.log_signal.connect(self.handle_automation_message)

        self.ui.tbStatus.clear()
        self.context.automation.start(self._loaded_points, dry_run=self.is_dry_run)

    def stop_modeling(self):
        if not self.context.automation or not self.context.automation.is_running:
            return

        self.context.automation.stop()

    def handle_automation_message(self, msg):
        self.append_log(msg)

    @property
    def is_dry_run(self):
        return self.ui.chkDryRun.isChecked()
