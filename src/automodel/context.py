from automodel.camera import Camera
from automodel.mount import Mount
from automodel.solver import PS2Solver
from automodel.settings import Settings
from automodel.automation import Automation


class Context(object):
    camera: Camera
    mount: Mount
    solver: PS2Solver
    settings: Settings
    automation: Automation

    def __init__(self):
        self.camera = None
        self.mount = None
        self.solver = None
        self.settings = None
        self.automation = None

    def close(self):
        if self.camera:
            self.camera.close()

        if self.mount:
            self.mount.close()

    def create_camera(self, prog_id):
        self.camera = Camera(prog_id)

    def create_mount(self, prog_id):
        self.mount = Mount(prog_id)

    def create_solver(self, ps2_directory):
        self.solver = PS2Solver(ps2_directory)

    def create_settings(self, q_settings):
        self.settings = Settings(q_settings)

    def create_automation(self):
        self.automation = Automation(self.camera, self.mount,
                                     self.solver, self.settings)
