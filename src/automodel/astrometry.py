import math
from astropy.coordinates.builtin_frames.utils import get_jd12, erfa
from astropy.coordinates import SkyCoord, EarthLocation, FK5, Angle
from astropy.time import Time
from astropy import units as u


class Astrometry(object):

    @staticmethod
    def transform_icrs_to_cirs(ra: float, dec: float, time: Time = None) -> SkyCoord:
        time = Time.now() if time is None else time
        jd1, jd2 = get_jd12(time, 'tdb')

        ra_rad = math.radians(ra * 15.0)
        dec_rad = math.radians(dec)

        ri, di, eo = erfa.atci13(ra_rad, dec_rad, 0, 0, 0, 0, jd1, jd2)
        ra = (ri - eo) % (2 * math.pi)
        dec = di
        return SkyCoord(ra=ra * u.rad, dec=dec * u.rad, frame='cirs', obstime=time)

    @staticmethod
    def degrees_to_dms(degrees):
        ang = Angle(degrees, unit=u.deg)
        return ang.to_string(precision=2, sep="dm")

    @staticmethod
    def hours_to_hms(hours):
        ang = Angle(hours, unit=u.hour)
        return ang.to_string(precision=2, sep="hm")

    @staticmethod
    def cartesian_to_altaz(xyz):
        x = xyz[0]
        y = xyz[1]
        z = xyz[2]
        zd = math.acos(z) * 180.0 / math.pi
        alt = 90.0 - zd
        if x == 0 and y == 0:
            # Special case for Up/Down vector
            az = 0
        else:
            az = 90.0 - math.atan2(y, x) * 180.0 / math.pi
        if az < 0:
            # atan2 is +/- 180 but azimuth convention is [0,360)
            az += 360.0
        return alt, az
