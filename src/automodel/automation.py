import tempfile
import os
import math
import traceback
import time
from threading import Thread
from typing import List

from PyQt5.QtCore import QObject, pyqtSignal
from astropy.coordinates import Angle
from astropy import units as u
from automodel.camera import Camera
from automodel.modelPoint import ModelPoint
from automodel.mount import Mount, ModelMode
from automodel.solver import PS2Solver
from automodel.settings import Settings


class Automation(QObject):
    log_signal = pyqtSignal(str)

    def __init__(self,
                 camera: Camera,
                 mount: Mount,
                 solver: PS2Solver,
                 settings: Settings):
        super(Automation, self).__init__()
        self.camera = camera
        self.mount = mount
        self.solver = solver
        self.settings = settings
        self._thread = None
        self.is_running = False
        self._request_stop = False

        # Options
        self._dry_run = False

    def start(self, model_points, dry_run=False):
        if self.is_running:
            return

        self._dry_run = dry_run

        self._thread = Thread(target=self._run, args=[model_points])
        self._thread.daemon = True
        self._thread.start()

    def stop(self):
        if not self.is_running:
            return

        self._log("Stopping...")
        self._request_stop = True

    def _run(self, model_points: List[ModelPoint]):
        self.is_running = True
        self._log("Starting")
        start_time = time.time()
        try:
            base_points = list(filter(lambda p: p.is_base, model_points))
            refine_points = list(filter(lambda p: not p.is_base, model_points))

            if len(base_points) != 3:
                self._log("There are not 3 base points, stopping")
                return

            self._unpark()
            self._clear_model()
            self._set_environmental()

            self._set_mode(ModelMode.BASE)
            for idx, point in enumerate(base_points):
                if self._request_stop:
                    return
                self._log(f"{idx + 1}/{len(base_points)} Base Points")
                self._solve_and_add(point)

            self._set_mode(ModelMode.REFINE)
            for idx, point in enumerate(refine_points):
                if self._request_stop:
                    return
                self._log(f"{idx + 1}/{len(refine_points)} Refine Points")
                self._solve_and_add(point)

            self._print_summary_table(model_points)
            self._park()
        except Exception as ex:
            print(ex)
            traceback.print_exc()
            self._log("An error occurred: " + str(ex))
        finally:
            elapsed_time = time.time() - start_time
            time_str = time.strftime("%M:%S", time.gmtime(elapsed_time))
            self._log(f"Elapsed time: {time_str}")

            if self._request_stop:
                self._log("Stopped")
            else:
                self._log("Complete")

            self.is_running = False
            self._request_stop = False

    def _solve_and_add(self, point: ModelPoint):
        self._slew(point)

        if self.settings.mount_settle_time > 0:
            self._log(f"Settle for {self.settings.mount_settle_time:.1f} seconds")
            time.sleep(self.settings.mount_settle_time)

        if self._request_stop:
            return

        data = self._take_image()

        if self._request_stop:
            return

        results = self._solve_image(data)
        if results:
            msg = self.solver.print_solver_results(results)
            for line in msg.splitlines():
                self._log(line)
            if not self._dry_run:
                self.mount.add_model_point_icrs(results.ra, results.dec)
        else:
            self._log("Failed to solve, moving on to next point")

    def _log(self, msg):
        self.log_signal.emit(msg)

    def _clear_model(self):
        if self._dry_run:
            return
        self._log("Clearing model on mount")
        self.mount.clear_model()

    def _set_mode(self, mode: ModelMode):
        if self._dry_run:
            return
        self.mount.set_mode(mode)

    def _set_environmental(self):
        if self._dry_run:
            return
        temperature = self.settings.temperature
        pressure = self.settings.pressure
        # Not yet implemented
        # self._log("Sending environmental data to mount")
        # self._log(f"Temperature = {temperature:.1f}, Pressure = {pressure:d}hPa")

    def _slew(self, point: ModelPoint):
        alt, az = point.to_tuple()
        ang_alt = Angle(alt, unit=u.deg)
        ang_az = Angle(az, unit=u.deg)
        self._log(f"Slewing to ALT: {ang_alt.to_string()} AZ: {ang_az.to_string()}")

        self.mount.slew_altaz(alt, az)

    def _park(self):
        if self._dry_run:
            return
        self.mount.park()

    def _unpark(self):
        if self._dry_run:
            return
        self.mount.unpark()

    def _take_image(self):
        binning = self.settings.camera_binning
        exposure_time = self.settings.camera_exposure
        self._log(f"Taking {binning}x{binning} image for {exposure_time} seconds")

        return self.camera.take_image(exposure_time, binning)

    def _solve_image(self, data):
        binning = self.settings.camera_binning
        scale = self.settings.camera_scale / 3600.0 / binning
        fov_w = self.camera.width * scale
        fov_h = self.camera.height * scale
        self._log("Solving image")

        # Get a temporary file, close the file descriptor so we can later delete it
        fd, filename = tempfile.mkstemp(suffix=".fits")
        os.close(fd)

        try:
            self.camera.save_fits(data, filename)

            ra = self.mount.ra * 15.0  # convert hours to degrees
            dec = self.mount.dec
            return self.solver.solve(ra, dec, fov_w, fov_h, filename)
        except Exception as ex:
            print(ex)
            traceback.print_exc()
            self._log("An error occurred during plate solve: " + str(ex))
        finally:
            os.remove(filename)

    def _print_summary_table(self, points):
        if self._dry_run:
            return

        rms = 0.0

        # TODO: use a table library to output the data
        self._log("*" * 53)
        for i, point in enumerate(points):
            try:
                error, polar_angle = self.mount.get_point_info(i + 1)
                rms += error ** 2
                alt, az = point.to_tuple()
                self._log("#%2d, err = %5.1f @ %3ddeg, Az %5.1f, Alt %5.1f" % (
                    i + 1, error, polar_angle, az, alt))
            except Exception as e:
                print(e)
        self._log("*" * 53)
        self._log("RMS error = %.2f" % (math.sqrt(rms / len(points))))
