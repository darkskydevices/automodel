class ModelPoint(object):
    def __init__(self,
                 alt: float,
                 az: float,
                 is_base=False):
        self.alt = alt
        self.az = az
        self.is_base = is_base

    def __repr__(self):
        return f"ALT: {self.alt:.2f}  AZ: {self.az:.2f}" + (" Base" if self.is_base else "")

    def __str__(self):
        return self.__repr__()

    def to_output(self):
        return f"{self.az:.2f}:{self.alt:.2f}" + (":BaseModelPoint" if self.is_base else "")

    def to_tuple(self):
        return self.alt, self.az

    @staticmethod
    def save(model_points, filename):
        with open(filename, "w") as f:
            for p in model_points:
                f.write(p.to_output())
                f.write("\n")

    @staticmethod
    def load(filename):
        with open(filename, "r") as f:
            for line in map(lambda s: s.split(":"), f.read().splitlines()):
                az = float(line[0])
                alt = float(line[1])
                point = ModelPoint(alt, az, len(line) == 3)
                yield point
