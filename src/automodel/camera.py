import time
from threading import Thread
import numpy as np
from astropy.io import fits
from PyQt5.QtCore import QObject, pyqtSignal
from win32com.client.dynamic import Dispatch
import pythoncom


class CameraPollingData(object):
    def __init__(self):
        self.is_connected = False
        self.temperature = float('nan')


class Camera(QObject):
    SIMULATOR_PROG_ID = "ASCOM.Simulator.Camera"
    QHY_PROG_ID = "ASCOM.QHYCCD.Camera"

    data_signal = pyqtSignal(CameraPollingData)

    def __init__(self, prog_id):
        super().__init__()
        self.prog_id = prog_id
        self.ascom = None
        self._has_cooler = False
        self._can_get_temperature = False
        self._can_set_temperature = False
        self._poller = None
        self._poll_interval = 0.5

    def _poll(self):
        # This method is dispatched on another thread
        # CoInitialize must be called to setup the thread
        # for Win32 COMObjects
        pythoncom.CoInitialize()

        while self.ascom:
            data = CameraPollingData()
            data.is_connected = self.is_connected

            if self.has_cooler and self._can_get_temperature:
                data.temperature = float(self.ascom.CCDTemperature)

            self.data_signal.emit(data)
            time.sleep(self._poll_interval)

        self.poller = None

    def _supports(self, func):
        try:
            func()
            return True
        except:
            return False

    def open(self):
        self.ascom = Dispatch(self.prog_id)
        self.ascom.Connected = True

        self._has_cooler = self._supports(lambda: self.ascom.CoolerOn)
        self._can_get_temperature = self._supports(lambda: self.ascom.CCDTemperature)
        self._can_set_temperature = self._supports(lambda: self.ascom.CanSetCCDTemperature)

        self._poller = Thread(target=self._poll)
        self._poller.daemon = True
        self._poller.start()

    def close(self):
        if self.is_connected and self.has_cooler:
            self.disable_cooler()

        self.ascom.Connected = False
        self.ascom.Dispose()
        self.ascom = None

    def take_image(self, exposure_time, binning=1):
        if not self.is_connected:
            return None
        if not (1 <= binning <= self.ascom.MaxBinX):
            return None
        if not (1 <= binning <= self.ascom.MaxBinY):
            return None
        if exposure_time <= 0:
            return None

        # Set binning and reset subframe
        self.ascom.BinX = binning
        self.ascom.BinY = binning
        self.ascom.StartX = 0
        self.ascom.StartY = 0
        self.ascom.NumX = int(self.width / float(binning))
        self.ascom.NumY = int(self.height / float(binning))
        self.ascom.StartExposure(float(exposure_time), True)
        while not self.ascom.ImageReady:
            time.sleep(0.1)

        return np.array(self.ascom.ImageArray, dtype='uint16')

    def _toggle_cooler(self, value):
        if not self.is_connected:
            return
        if not self.has_cooler:
            return

        self.ascom.CoolerOn = value

    def enable_cooler(self):
        self._toggle_cooler(True)

    def disable_cooler(self):
        self._toggle_cooler(False)

    def set_temperature(self, temperature):
        disqualifiers = [
            not self.is_connected,
            not self._has_cooler,
            not self._can_set_temperature,
        ]
        if any(disqualifiers):
            return

        if not bool(self.ascom.CoolerOn):
            self.enable_cooler()

        self.ascom.SetCCDTemperature = temperature

    @staticmethod
    def save_fits(data, filename):
        # data is an np.array, dim0 is width and dim1 is height
        # fits needs the data flipped and rotated
        header = fits.header.Header()
        fits.writeto(filename, np.rot90(np.flip(data, 1)), header=header, overwrite=True)

    @property
    def is_connected(self):
        return self.ascom.Connected

    @property
    def width(self):
        return self.ascom.CameraXSize if self.is_connected else None

    @property
    def height(self):
        return self.ascom.CameraYSize if self.is_connected else None

    @property
    def has_cooler(self):
        return self._has_cooler

    @property
    def temperature(self):
        if not self.is_connected:
            return False
        if self._can_get_temperature:
            return float(self.ascom.CCDTemperature)
        return float('nan')
