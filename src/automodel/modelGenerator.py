from enum import IntEnum
import math

from automodel.astrometry import Astrometry
from automodel.modelPoint import ModelPoint


class ModelType(IntEnum):
    CARTESIAN = 0
    EQUIDISTANT = 1


class ModelGenerator(object):
    def __init__(self, model_type: ModelType):
        self.model_type = model_type
        self.min_alt = 0.0
        self.min_az = 0.0
        self.max_alt = 90.0
        self.max_az = 360.0
        self.rows = 0
        self.cols = 0
        self.num = 0

    def generate(self):
        if self.model_type == ModelType.CARTESIAN:
            return self._generate_cartesian()
        if self.model_type == ModelType.EQUIDISTANT:
            return self._generate_equidistant()
        raise RuntimeError("Unknown model type")

    def _generate_cartesian(self):
        az_step = (self.max_az - self.min_az) / self.cols if self.cols > 0 else 0
        alt_step = (self.max_alt - self.min_alt) / (self.rows - 1) if self.rows > 1 else 0

        for x in range(0, self.cols):
            for y in range(0, self.rows):
                az = self.min_az + az_step * x
                alt = self.min_alt + alt_step * y
                yield ModelPoint(alt, az)

    def _generate_equidistant(self):
        n = int(1.9 * self.num)
        points = []

        def xyz_to_modelpoint(v):
            a, b = Astrometry.cartesian_to_altaz(v)
            return ModelPoint(a, b)

        while True:
            xyz = self._equidistant(n, 0.1 + 1.2 * n)
            for v in xyz:
                alt, az = Astrometry.cartesian_to_altaz(v)
                if self.min_alt <= alt <= self.max_alt:
                    points.append(v)
            if len(points) >= self.num:
                model_points = map(lambda p: xyz_to_modelpoint(p), points)
                return sorted(model_points, key=lambda mp: mp.az)
            n += 1
            points.clear()

    @staticmethod
    def _equidistant(n, a):
        # Kogan, Jonathan (2017) "A New Computationally Efficient Method for Spacing n Points on a Sphere," Rose-Hulman Undergraduate
        # Mathematics Journal: Vol. 18 : Iss. 2 , Article 5.
        # Available at: https://scholar.rose-hulman.edu/rhumj/vol18/iss2/5
        def spherical(x, y):
            return [math.cos(x) * math.cos(y), math.sin(x) * math.cos(y), math.sin(y)]

        pts = []
        start = -1.0 + 1.0 / (n - 1.0)
        increment = (2.0 - 2.0 / (n - 1.0)) / (n - 1.0)
        for j in range(0, n):
            s = start + j * increment
            pts.append(
                spherical(s * a,
                          math.pi / 2.0 * math.copysign(1, s) * (1.0 - math.sqrt(1.0 - abs(s))))
            )
        return pts
