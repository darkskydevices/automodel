import matplotlib.pyplot as plt
from matplotlib import cm, colors
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import math
from astropy.coordinates import AltAz

deg2rad = np.pi / 180.0
rad2deg = 180.0 / np.pi

def spherical_coordinate(x, y):
    return np.array([math.cos(x) * math.cos(y), math.sin(x) * math.cos(y), math.sin(y)])

def to_altaz(xyz):
    ptsnew = np.zeros((xyz.shape[0], 2))
    ptsnew[:,0] = np.arctan2(xyz[:,2], np.sqrt(xyz[:,0]**2 + xyz[:,1]**2))
    ptsnew[:,1] = np.arctan2(xyz[:,1], xyz[:,0]) + 2.0 * np.pi
    return ptsnew

def altaz(xyz):
    x = xyz[0]
    y = xyz[1]
    z = xyz[2]
    zd = math.acos(z) * 180.0 / np.pi
    alt = 90.0 - zd
    az = 90.0 - math.atan2(y, x) * 180.0 / np.pi
    if az < 0:
        az += 360.0
    return alt, az

def NX(n, x):
    # Kogan, Jonathan (2017) "A New Computationally Efficient Method for Spacing n Points on a Sphere," Rose-Hulman Undergraduate
    # Mathematics Journal: Vol. 18 : Iss. 2 , Article 5.
    # Available at: https://scholar.rose-hulman.edu/rhumj/vol18/iss2/5
    pts = []
    start = -1.0 + 1.0 / (n - 1.0)
    increment = (2.0 - 2.0 / (n - 1.0)) / (n - 1.0)
    for j in range(0, n):
        s = start + j * increment
        pts.append(spherical_coordinate(s*x, math.pi / 2.0 * math.copysign(1, s) * (1.0 - math.sqrt(1.0 - abs(s)))))
    return pts


def find_points(count, min_alt, max_alt):
    n = int(1.9 * count)
    points = []
    while True:
        xyz = NX(n, 0.1 + 1.2 * n)
        for p in xyz:
            alt, az = altaz(p)
            if min_alt <= alt <= max_alt:
                points.append(p)
        if len(points) >= count:
            return n, points
        n += 1
        points.clear()
        
n, data = find_points(97, 30, 70)

altaz_data = [altaz(p) for p in data]
sorted_altaz_data = sorted(altaz_data, key=lambda p: p[1])

# print(f"Final n = {n}")
# for alt, az in sorted_altaz_data:
#     print(f"({az:.10f},{alt:.10f}),")

# Create a sphere
r = 1
pi = np.pi
cos = np.cos
sin = np.sin
phi, theta = np.mgrid[0.0:pi:100j, 0.0:2.0*pi:100j]
x = r*sin(phi)*cos(theta)
y = r*sin(phi)*sin(theta)
z = r*cos(phi)

npdata = np.array(data)
xx, yy, zz = np.hsplit(npdata, 3) 

altaz = to_altaz(npdata)
alt, az = np.hsplit(altaz, 2)
zd = 90 - (alt * 180.0 / math.pi) # altitude to zenith distance, must be radians (thanks matplotlib)

fig = plt.figure()

ax = fig.add_subplot(121, projection='3d')
ax.plot_surface(x, y, z,  rstride=1, cstride=1, color='c', alpha=0.2, linewidth=0)
ax.scatter(xx,yy,zz,color="k",s=15)
ax.set_xlim([-1,1])
ax.set_ylim([-1,1])
ax.set_zlim([-1,1])
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_zticklabels([])
ax.set_aspect("equal")
# ax.view_init(elev=-90., azim=0)

ax2 = fig.add_subplot(122, polar=True)
ax2.scatter(az, zd)
ax2.set_theta_zero_location("N")
ax2.set_rlim(1, 91)
ax2.set_xticklabels(["N", "NE", "E", "SE", "S", "SW", "W", "NW"])
# ax2.set_yticklabels(map(lambda x: str(x), range(80, -10, -10)))
ax2.set_yticklabels([])
ax2.grid(True, which="major")
ax2.set_aspect("equal")

plt.tight_layout()
plt.show()