$uiDir = Resolve-Path "../src/automodel/ui"

Get-ChildItem $uiDir -Filter *.ui |
        ForEach-Object {
            $filename = Split-Path $_.FullName -Leaf
            $filenameNoExt = [io.path]::GetFileNameWithoutExtension($filename)
            $outputFilename = "$( $uiDir )/ui_$( $filenameNoExt ).py"
            python -m PyQt5.uic.pyuic -x $_.FullName -o $outputFilename
        }