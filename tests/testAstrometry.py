import unittest
import math
from datetime import datetime
from automodel.astrometry import Astrometry
from astropy.time import Time
from astropy.coordinates import SkyCoord, FK5, Angle
from astropy import units as u


class TestAstrometry(unittest.TestCase):

    def test_transform_icrs_to_cirs(self):
        time = Time(datetime(2020, 4, 23, 5, 25, 0), scale='utc')

        icrs_coord = SkyCoord('13h 24m 33.5s -5d 9m 50s', unit=(u.hourangle, u.deg), frame='icrs')
        cirs_coord = SkyCoord('13h 25m 37s -5d 16m 10s', unit=(u.hourangle, u.deg), frame='cirs', obstime=time)

        result = Astrometry.transform_icrs_to_cirs(icrs_coord.ra.hour, icrs_coord.dec.deg, time)
        self.assertTrue(result.separation(cirs_coord) < Angle('1.0"'))

    def test_cartesian_to_altaz(self):
        # XYZ aligns with East, North, Up
        deg30 = math.sin(math.pi / 6.0)
        deg45 = 1.0 / math.sqrt(2)
        deg60 = math.sin(math.pi / 3.0)
        north = [0, 1, 0]
        east = [1, 0, 0]
        south = [0, -1, 0]
        west = [-1, 0, 0]
        up = [0, 0, 1]
        down = [0, 0, -1]
        north_east_alt30 = [deg45, deg45, deg30]
        north_east_alt45 = [deg45, deg45, deg45]
        north_east_alt60 = [deg45, deg45, deg60]

        def assert_it(expected_alt, expected_az, vector, precision=10):
            alt, az = Astrometry.cartesian_to_altaz(vector)
            self.assertAlmostEqual(expected_alt, alt, precision)
            self.assertAlmostEqual(expected_az, az, precision)

        assert_it(0, 0, north)
        assert_it(0, 90, east)
        assert_it(0, 180, south)
        assert_it(0, 270, west)
        assert_it(90, 0, up)
        assert_it(-90, 0, down)
        assert_it(30, 45, north_east_alt30)
        assert_it(45, 45, north_east_alt45)
        assert_it(60, 45, north_east_alt60)
