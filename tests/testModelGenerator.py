import unittest
from automodel.modelGenerator import ModelGenerator, ModelType


class TestModelGenerator(unittest.TestCase):

    def test_cartesian_generate(self):
        g = ModelGenerator(ModelType.CARTESIAN)
        g.rows = 3
        g.cols = 5
        g.min_alt = 55
        g.max_alt = 75
        g.min_az = 0
        g.max_az = 360

        # pull out just the az and alt coords
        points = list(map(lambda p: (p.az, p.alt), g.generate()))

        # (AZ, ALT)
        expected = [
            (0.0, 55.0),
            (0.0, 65.0),
            (0.0, 75.0),
            (72.0, 55.0),
            (72.0, 65.0),
            (72.0, 75.0),
            (144.0, 55.0),
            (144.0, 65.0),
            (144.0, 75.0),
            (216.0, 55.0),
            (216.0, 65.0),
            (216.0, 75.0),
            (288.0, 55.0),
            (288.0, 65.0),
            (288.0, 75.0),
        ]

        self.assertEqual(15, len(points))
        self.assertListEqual(expected, points)

    def test_equidistant_generate(self):
        g = ModelGenerator(ModelType.EQUIDISTANT)
        g.min_alt = 20
        g.max_alt = 60
        g.num = 15

        # pull out just the az and alt coords
        points = list(map(lambda p: (p.az, p.alt), g.generate()))

        # (AZ, ALT)
        expected = [
            (10.2012546615, 44.2105263158),
            (31.5217180432, 24.1936863658),
            (63.1964284296, 35.8303680050),
            (94.8711388160, 50.7797610051),
            (116.1916021978, 28.5832094565),
            (147.8663125842, 41.2567925039),
            (169.1867759660, 22.1052631579),
            (179.5410229706, 58.6985879975),
            (200.8614863524, 33.3116113547),
            (232.5361967387, 47.3684210526),
            (253.8566601205, 26.3505967240),
            (285.5313705069, 38.4720982987),
            (306.8518338887, 20.0791900248),
            (317.2060808933, 54.5175709670),
            (338.5265442751, 30.9001033509),
        ]

        self.assertEqual(15, len(points))

        # ListAlmostEqual doesnt have precision
        for e, p in zip(expected, points):
            self.assertAlmostEqual(e[0], p[0], 10)
            self.assertAlmostEqual(e[1], p[1], 10)
