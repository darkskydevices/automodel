import unittest

from automodel.modelPoint import ModelPoint


class TestModelPoint(unittest.TestCase):

    def test_base_point_to_string(self):
        p = ModelPoint(1.234, 5.678, True)
        expected = "ALT: 1.23  AZ: 5.68 Base"
        self.assertEqual(expected, str(p))

    def test_refine_point_to_string(self):
        p = ModelPoint(1.234, 5.678, False)
        expected = "ALT: 1.23  AZ: 5.68"
        self.assertEqual(expected, str(p))

    def test_base_point_to_output(self):
        p = ModelPoint(1.234, 5.678, True)
        expected = "5.68:1.23:BaseModelPoint"
        self.assertEqual(expected, p.to_output())

    def test_base_point_to_output(self):
        p = ModelPoint(1.234, 5.678, False)
        expected = "5.68:1.23"
        self.assertEqual(expected, p.to_output())
